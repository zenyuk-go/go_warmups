package solution

// you can also use imports, for example:
// import "fmt"
// import "os"

// you can write to stdout for debugging purposes, e.g.
// fmt.Println("this is a debug message")

//type elementData struct {
//    index int
//    value int
//    minDiff int
//    minDistance int
//}

func Solution(A []int) int {
    // write your code in Go 1.4
    if len(A) < 2 {
        return -1
    }
    
    // fill in elementData
    //allData := map[int]elementData{}
    //for idx, v := range A {
    //    allData[idx] = elementData{idx, v, 0, 0}
    //}
    
    minDiff := math.Abs(A[0] - A[])
    minDistance := 1
    resultIdxA := 0
    resultIdxB := 1
    for idx, el := range A {
        for curIdx, curElement := range A {
            if curIdx == idx || A[curIdx] == A[idx] {
                continue
            }
            if math.Abs(A[curIdx] - A[idx]) < minDiff {
                resultIdxA = curIdx
                resultIdxB = idx
            }
        }    
    }
    
    return math.Abs(resultIdxA - resultIdxB)
}