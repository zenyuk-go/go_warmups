package main

import (
	"fmt"
	"strings"
)

// Node is a tree node
type Node struct {
	value int
	left  *Node
	right *Node
}

// ReverseTree - mirror left & right sides of the tree
func (n *Node) ReverseTree() {
	temp := &Node{}
	temp = n.left
	n.left = n.right
	n.right = temp
	if n.left != nil {
		n.left.ReverseTree()
	}
	if n.right != nil {
		n.right.ReverseTree()
	}
}

// PrintLeaves - prints all it's leaves
func (n *Node) PrintLeaves() {
	n.printLeaves(0, true)
}
func (n *Node) printLeaves(depth int, right bool) {
	var spaces = strings.Repeat(" ", depth)
	var template string
	if right {
		template = "%s└-%d \n"
	} else {
		template = "%s├-%d \n"
	}
	fmt.Printf(template, spaces, n.value)
	if n.left != nil {
		n.left.printLeaves(depth+1, false)
	}
	if n.right != nil {
		n.right.printLeaves(depth+1, true)
	}
}
