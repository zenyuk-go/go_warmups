package main

func main() {
 	const limit = 4000000
	current := 2
	previous := 1
	sum := 0
	for current < limit {
		if current % 2 == 0 {
			sum += current
		}
		previous, current = current, previous + current
	}
	println(sum)
}